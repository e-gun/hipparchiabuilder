__all__ = ['idtfiles', 'regex_substitutions', 'swappers', 'lexica',
           'betacodelowercase', 'citation_builder', 'betacodecapitals', 'betacodeescapedcharacters',
           'betacodefontshifts', 'betacodeandunicodeinterconversion', 'copticsubstitutions']